<?php
/**
 * Il file base di configurazione di WordPress.
 *
 * Questo file viene utilizzato, durante l’installazione, dallo script
 * di creazione di wp-config.php. Non è necessario utilizzarlo solo via
 * web, è anche possibile copiare questo file in «wp-config.php» e
 * riempire i valori corretti.
 *
 * Questo file definisce le seguenti configurazioni:
 *
 * * Impostazioni MySQL
 * * Prefisso Tabella
 * * Chiavi Segrete
 * * ABSPATH
 *
 * È possibile trovare ultetriori informazioni visitando la pagina del Codex:
 *
 * @link https://codex.wordpress.org/it:Modificare_wp-config.php
 *
 * È possibile ottenere le impostazioni per MySQL dal proprio fornitore di hosting.
 *
 * @package WordPress
 */

// ** Impostazioni MySQL - È possibile ottenere queste informazioni dal proprio fornitore di hosting ** //
/** Il nome del database di WordPress */
define('DB_NAME', 'bassafedelta');

/** Nome utente del database MySQL */
define('DB_USER', 'root');

/** Password del database MySQL */
define('DB_PASSWORD', 'ft2ft2');

/** Hostname MySQL  */
define('DB_HOST', 'localhost');

/** Charset del Database da utilizzare nella creazione delle tabelle. */
define('DB_CHARSET', 'utf8mb4');

/** Il tipo di Collazione del Database. Da non modificare se non si ha idea di cosa sia. */
define('DB_COLLATE', '');

/**#@+
 * Chiavi Univoche di Autenticazione e di Salatura.
 *
 * Modificarle con frasi univoche differenti!
 * È possibile generare tali chiavi utilizzando {@link https://api.wordpress.org/secret-key/1.1/salt/ servizio di chiavi-segrete di WordPress.org}
 * È possibile cambiare queste chiavi in qualsiasi momento, per invalidare tuttii cookie esistenti. Ciò forzerà tutti gli utenti ad effettuare nuovamente il login.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '[`S7-v.)ZA3f(-[]z|z-e83xPb|-&b~_0x9d-^RnL&)LYE0Bq-$>.IWp=}D/NuYb');
define('SECURE_AUTH_KEY',  '-UXC/`1Az;|{67H9UWiGP1-R-$Xe62+~?CC}o*X2bULnvGBFU tfY+bu6,c1zsaw');
define('LOGGED_IN_KEY',    '(@D]$@K_)>? cn#{S|eHFw+wyoaMXB~l5*-oWWM7![|7.oUjqJ@wAb95-f:C.YpH');
define('NONCE_KEY',        '_BIlsiZ;/!w[x!4Xi-yoT!45e:|DG$?/IykR^omRU}S?-^5fCU/CDPB3uE-#OV91');
define('AUTH_SALT',        'KargSv%Sa)A| L,kP/w726I&(tu2Ql9,bf([KH9{39xh&-uw[^T0&Ou_imr==eg4');
define('SECURE_AUTH_SALT', 'ovIY)~S4=j6*<Li22t8tArS3#2Kz#N>VomUn+H$K]i?@uAE{bn(stk#{asre.%|O');
define('LOGGED_IN_SALT',   '[xeu?18dB~DR|Lv-/MVVr/R+uPix{RtBQM`?-<Vt[iwlt<ivU`xiiA!u m$~;1au');
define('NONCE_SALT',       'ffDT[1@h+Oe?`=[2d|nEXI#gPcP}/y4dCm-f0298`zn=TF)-8MKZg.3u5fdd9Fqv');

/**#@-*/

/**
 * Prefisso Tabella del Database WordPress.
 *
 * È possibile avere installazioni multiple su di un unico database
 * fornendo a ciascuna installazione un prefisso univoco.
 * Solo numeri, lettere e sottolineatura!
 */
$table_prefix  = 'bf_';

/**
 * Per gli sviluppatori: modalità di debug di WordPress.
 *
 * Modificare questa voce a TRUE per abilitare la visualizzazione degli avvisi
 * durante lo sviluppo.
 * È fortemente raccomandato agli svilupaptori di temi e plugin di utilizare
 * WP_DEBUG all’interno dei loro ambienti di sviluppo.
 */
define('WP_DEBUG', false);

/* Finito, interrompere le modifiche! Buon blogging. */

/** Path assoluto alla directory di WordPress. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Imposta le variabili di WordPress ed include i file. */
require_once(ABSPATH . 'wp-settings.php');

define('FS_METHOD', 'direct');
