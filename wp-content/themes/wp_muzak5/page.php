<?php get_header(); ?>
<?php get_template_part('inc_section'); ?>

<div class="row">
	<div class="sixteen columns">
		
		<?php
			if( woocommerce_enabled() and ( is_cart() or is_checkout() or is_account_page() ) )
				$cols = 'sixteen omega';
			else
				$cols = 'twelve';
		?>

		<div class="<?php echo $cols; ?> columns content alpha">
		
			<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
	
				<article <?php post_class('static'); ?>>
		
					<div class="post-body hyphenate">
						<?php the_post_thumbnail('ci_fullwidth', array('class' => 'scale-with-grid img-fullwidth')); ?>
						<?php the_content(); ?>
						<?php wp_link_pages(); ?>
					</div>
	
					<?php comments_template(); ?> 
				</article><!-- /single -->
				
			<?php endwhile; endif; ?>
		
		</div><!-- /twelve columns -->
		
		<?php if( woocommerce_enabled() and ( is_cart() or is_checkout() or is_account_page() ) ):?>
			<?php // Do nothing ?>
		<?php else: ?>
			<aside class="four columns sidebar omega">
				<?php dynamic_sidebar('pages-sidebar'); ?>
			</aside <!-- #sidebar -->
		<?php endif; ?>

	</div><!-- /sixteen columns -->
</div><!-- /row -->		

<?php get_footer(); ?>