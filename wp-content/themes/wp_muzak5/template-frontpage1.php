<?php
/*
Template Name: Homepage (Sidebar #1 / Content / Sidebar #2)
*/
?>

<?php get_header(); ?>

<?php get_template_part('inc_slider'); ?>

<!-- ########################### MAIN ########################### -->
<div class="row">
	<div class="sixteen columns">


		<aside class="four columns alpha sidebar">
			<?php dynamic_sidebar('homepage-sidebar-one'); ?>
		</aside><!-- /sidebar -->

		<div class="eight columns content">

			<?php if (ci_setting('homepage-page-id') == ""): ?>

				<h3 class="widget-title"><?php _e('News','ci_theme'); ?></h3>
				<?php
					$args = array(
						'post_type'      => 'post',
						'paged'          => ci_get_page_var(),
						'posts_per_page' => ci_setting( 'news-no' ),
						'cat'            => ci_setting( 'news-cat' )
					);
					$q = new WP_Query( $args );
				?>
				<?php while ( $q->have_posts() ) : $q->the_post(); ?>
					<article class="post group">
						<div class="post-intro">
							<?php
								if ( ci_setting( 'featured_single_show' ) == 'enabled' ) {
									$attr = array( 'class' => "scale-with-grid" );
									the_post_thumbnail( 'ci_home_listing_short', $attr );
								}
							?>
							<h2><a href="<?php the_permalink(); ?>" title="<?php echo esc_attr( sprintf( __( 'Permalink to %s', 'ci_theme' ), get_the_title() ) ); ?>"><?php the_title(); ?></a></h2>
						</div><!-- /intro -->
						<div class="post-body">
							<p class="meta"><time class="post-date" datetime="<?php echo esc_attr( get_the_date( 'c' ) ); ?>"><?php echo get_the_date(); ?></time> <span class="bull">&bull;</span> <a href="<?php comments_link(); ?>"><?php comments_number(); ?></a><span class="bull">&bull;</span><?php the_category(' '); ?></p>
							<?php the_excerpt(); ?>
						</div>
					</article><!-- /post -->
				<?php endwhile; ?>

				<?php ci_pagination( array(), $q ); ?>
				<?php wp_reset_postdata(); ?>

			<?php else: ?>

				<?php
					$the_page = new WP_Query( array(
						'page_id' => ci_setting( 'homepage-page-id' )
					) );
				?>
				<?php while ( $the_page->have_posts() ) : $the_page->the_post(); ?>
					<h3 class="widget-title"><?php the_title(); ?></h3>
					<article class="post group">
						<div class="post-intro">
							<?php
								if ( ci_setting( 'featured_single_show' ) == 'enabled' ) {
									$attr = array( 'class' => "scale-with-grid" );
									the_post_thumbnail( 'ci_home_listing_short', $attr );
								}
							?>
						</div><!-- /intro -->
						<div class="post-body post-page-id">
							<?php the_content(); ?>
						</div>
					</article><!-- /post -->
				<?php endwhile; ?>
				<?php wp_reset_postdata(); ?>

			<?php endif; ?>

		</div>

		<aside class="four columns omega sidebar">
			<?php dynamic_sidebar('homepage-sidebar-two'); ?>
		</aside><!-- /sidebar -->

	</div><!-- /sixteen columns -->
</div><!-- /row -->

<?php get_footer(); ?>