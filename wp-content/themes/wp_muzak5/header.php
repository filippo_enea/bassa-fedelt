<!DOCTYPE html>
<!--[if lt IE 7 ]><html class="ie ie6" <?php language_attributes(); ?>> <![endif]-->
<!--[if IE 7 ]><html class="ie ie7" <?php language_attributes(); ?>> <![endif]-->
<!--[if IE 8 ]><html class="ie ie8" <?php language_attributes(); ?>> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><!--><html <?php language_attributes(); ?>> <!--<![endif]-->
<head>

	<!-- Basic Page Needs
	================================================== -->
	<meta charset="<?php bloginfo( 'charset' ); ?>">

	<!-- Mobile Specific Metas 
	================================================== -->
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

	<?php wp_head(); ?>

</head>

<body <?php body_class(); ?>>
<?php do_action('after_open_body_tag'); ?>

<?php get_template_part('inc_mobile_nav'); ?>

<div id="wrap">
	<div class="container">
		
		<!-- ########################### HEADER ########################### -->
		<header id="header" class="group">

			<div id="logo" class="four columns <?php logo_class(); ?>">
				<?php ci_e_logo('<h1>', '</h1>'); ?>
			</div>

			<nav id="nav" class="nav twelve columns">
				<?php 
					if(has_nav_menu('ci_main_menu'))
						wp_nav_menu( array(
							'theme_location' 	=> 'ci_main_menu',
							'fallback_cb' 		=> '',
							'container' 		=> '',
							'menu_id' 			=> 'navigation',
							'menu_class' 		=> 'sf-menu group'
						));
					else
						wp_page_menu();
				?>
			</nav><!-- /nav -->

			<nav id="cart-nav" class="nav twelve columns">
				<?php 
					if(has_nav_menu('ci_commerce_menu'))
						wp_nav_menu( array(
							'theme_location' 	=> 'ci_commerce_menu',
							'fallback_cb' 		=> '',
							'container' 		=> '',
							'menu_id' 			=> 'navigation-commerce',
							'menu_class' 		=> 'sf-menu group'
						));
					else
						wp_page_menu();
				?>
			</nav><!-- /nav -->

		</header><!-- /header -->	