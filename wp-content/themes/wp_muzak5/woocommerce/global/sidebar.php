<?php
/**
 * Sidebar
 *
 * @author 		CSSIgniter
 * @version     1.6.4
 */
?>

<div class="four columns omega sidebar">
	<?php dynamic_sidebar('shop-sidebar'); ?>
</div> <!-- #sidebar -->